---
apiVersion: policy/v1beta1
kind: PodDisruptionBudget
metadata:
  name: elasticsearch-data
  namespace: logging
spec:
  maxUnavailable: 1
  selector:
    matchLabels:
      app: elasticsearch
      component: data
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: elasticsearch-data
  namespace: logging
spec:
  replicas: {{ .Services.Logging.Replicas }}
  updateStrategy:
    type: RollingUpdate
  podManagementPolicy: Parallel
  serviceName: elasticsearch-data
  selector:
    matchLabels:
      app: elasticsearch
      component: data
  template:
    metadata:
      labels:
        app: elasticsearch
        component: data
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: component
                  operator: In
                  values:
                  - data
              topologyKey: kubernetes.io/hostname
            weight: 100
      initContainers:
      # see https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html
      # and https://www.elastic.co/guide/en/elasticsearch/reference/current/setup-configuration-memory.html#mlockall
      - name: "sysctl"
        image: "busybox:1.30.0-uclibc"
        imagePullPolicy: "Always"
        command: ["sysctl", "-w", "vm.max_map_count=262144"]
        securityContext:
          privileged: true
      - name: "chown-a"
        image: "docker.elastic.co/elasticsearch/elasticsearch-oss:6.4.2"
        command:
        - /bin/bash
        - -c
        - chown -R elasticsearch:elasticsearch /usr/share/elasticsearch/data &&
          chown -R elasticsearch:elasticsearch /usr/share/elasticsearch/logs
        securityContext:
          runAsUser: 0
        volumeMounts:
        - mountPath: /usr/share/elasticsearch/data
          name: data
      containers:
      - name: elasticsearch
        env:
        - name: NODE_MASTER
          value: "false"
        - name: PROCESSORS
          valueFrom:
            resourceFieldRef:
              resource: limits.cpu
        - name: ES_JAVA_OPTS
          value: "-Djava.net.preferIPv4Stack=true -Xms1536m -Xmx1536m"
        - name: DISCOVERY_HOSTS
          value: elasticsearch-master-0.elasticsearch-master,elasticsearch-master-1.elasticsearch-master,elasticsearch-master-2.elasticsearch-master
        image: "docker.elastic.co/elasticsearch/elasticsearch-oss:6.4.2"
        ports:
        - containerPort: 9300
          name: transport
        resources:
          limits:
            memory: "3500Mi"
          requests:
            cpu: "100m"
            memory: "3500Mi"
        readinessProbe:
          httpGet:
            path: /_cluster/health?local=true
            port: 9200
          initialDelaySeconds: 5
        volumeMounts:
        - mountPath: /usr/share/elasticsearch/data
          name: data
        - mountPath: /usr/share/elasticsearch/config/elasticsearch.yml
          name: config
          subPath: elasticsearch.yml
        - mountPath: /usr/share/elasticsearch/config/logging.yml
          name: config
          subPath: logging.yml
        - name: config
          mountPath: /pre-stop-hook.sh
          subPath: pre-stop-hook.sh
        - name: config
          mountPath: /post-start-hook.sh
          subPath: post-start-hook.sh
        lifecycle:
          preStop:
            exec:
              command: ["/bin/bash","/pre-stop-hook.sh"]
          postStart:
            exec:
              command: ["/bin/bash","/post-start-hook.sh"]
      terminationGracePeriodSeconds: 3600
      volumes:
      - name: config
        configMap:
          name: elasticsearch
  volumeClaimTemplates:
    - metadata:
        name: data
      spec:
        accessModes:
          - ReadWriteOnce
        resources:
          requests:
            storage: 25Gi