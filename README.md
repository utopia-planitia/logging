# EFK Stack

A deployment of [Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html), [Fluentd](https://docs.fluentd.org/v1.0/articles/quickstart) abd [Kibana](https://www.elastic.co/guide/en/kibana/6.1/index.html).

Supportive tools are [Cerebro](https://github.com/lmenezes/cerebro) and [Curator](https://github.com/elastic/curator).

## Usage

run `make` for usage

## Access

Kibana: via ingress

Cerebro: via ingress

Elasticsearch:

- run `make proxy` to forward apiserver to port `http://127.0.0.1:8001`
- access `http://localhost:8001/api/v1/namespaces/logging/services/elasticsearch:http/proxy`

## Update

### Sources
- https://github.com/helm/charts/blob/master/incubator/elasticsearch
- https://github.com/kubernetes/kubernetes/blob/master/cluster/addons/fluentd-elasticsearch/es-statefulset.yaml
- https://github.com/helm/charts/tree/master/stable/cerebro
- https://github.com/helm/charts/tree/master/incubator/elasticsearch-curator

### Verify
- check that cerebro has client, data and master nodes connected
- check kibana shows up to date logs
